# Assesment Test Software Engineer ( Backend ) at Gibox Digital

![GIBOX](https://www.giboxdigital.com/assets/img/gibox-white.png)

**[Gibox Digital Asia](https://github.com/LintangWisesa/GIBOX-JC05-03_BackEndWeb)**

#

### **Soal 1 - MySQL Database**

1. Tuliskan langkah-langkah/urutan query MySQL untuk membuat sebuah database **"Toko"** yang memiliki tabel **"Produk"** dengan data sebagai berikut:

   ![Soal_1.1](https://3.bp.blogspot.com/-7hiJnB9dKxs/W0U6tSXbP0I/AAAAAAAAERU/mwIGEQ9rQZoIzpucw16AHha_iJB3-IXgACLcBGAs/s1600/Soal_2a.png)

2. Dari tabel **"Produk"** di database **"Toko"**, tuliskan query MySQL untuk menampilkan seluruh data, dikelompokkan berdasarkan kolom _**kota**_. Hasil yang diharapkan sebagai berikut:

   ![Soal_1.2](https://4.bp.blogspot.com/-h3qI7a1HNfc/W0U6tWaKbiI/AAAAAAAAERY/dKGTXNQphp8FpSLe9vquAc4scWbzB3ohQCLcBGAs/s1600/Soal_2b.png)

3. Dari tabel **"Produk"** di database **"Toko"**, tuliskan query MySQL untuk menampilkan data produk dengan _**harga tertinggi di setiap kota**_, kemudian diurutkan berdasarkan kolom _**harga**_. Hasil yang diharapkan sebagai berikut:

   ![Soal_1.3](https://1.bp.blogspot.com/-BWCRTITLkEQ/W0U6tJUMWZI/AAAAAAAAERQ/ukGByJT5nsow29coBgdZkZPFlKst6oQXwCLcBGAs/s1600/Soal_2c.png)

4. Dari tabel **"Produk"** di database **"Toko"**, tuliskan query MySQL untuk menampilkan data produk dengan _**harga terendah**_. Hasil yang diharapkan sebagai berikut:

   ![Soal_1.4](https://2.bp.blogspot.com/-hIW5cFVDRRo/W0U6uMDG9ZI/AAAAAAAAERc/LsTu2LmW-okuHw4MAQ65rIVmtd9C2XjfACLcBGAs/s1600/Soal_2d.png)

   _**Catatan:**_ _Soal ini hanya meminta Anda untuk menuliskan langkah-langkah/urutan query MySQL sesuai spesifikasi di atas. Ketik jawaban dalam sebuah file **.txt** & lampirkan via email!_

#

### **Soal 2 - Express or Python & MySQL**

Buatlah sebuah project back-end NodeJS (Express) or Python sederhana yang mampu mengakses database MySQL, dengan spesifikasi route sebagai berikut:

- **_POST /karyawan_** &rarr; akan memasukkan data ke tabel **"karyawan"** di database **"toko"**. Data yang diinput via _**body request**_ hanyalah **nama** dan **tglLahir** (dalam _**JSON**_), sebagai contoh:

  ```json
  {
    "nama": "Lintang",
    "tglLahir": "26-11-1992"
  }
  ```

  Kemudian hasil yang tersimpan di tabel **"karyawan"** tersusun atas kolom **no, nama, hari, bulan, tahun, zodiak** dan **usia**. Hasil yang diharapkan sebagai berikut:

  ![Soal_4](https://3.bp.blogspot.com/-74OaRJ5oM90/W0VJq7Qm8aI/AAAAAAAAER0/HOEF0qreMWIrowzHDNqb4wkqOXver1TNQCLcBGAs/s1600/Soal4.png)

- **_GET /karyawan_** &rarr; akan memberikan response: menampilkan semua data dari tabel **"karyawan"** di database **"toko"**. Adapun klasifikasi kolom **zodiak** disajikan pada gambar berikut:

  ![zodiak](https://1.bp.blogspot.com/-fZAEel3MI-s/W0VkJFrz-vI/AAAAAAAAESA/FSgalYKRydg0fBQAzYUFrHd2PhDWcswzACLcBGAs/s1600/zodiak_20151128_202626.jpg)

  _**Catatan:**_ _Upload source code project ke **Github** Anda, kemudian ketik kode jawaban dalam sebuah file **.txt**. Lampirkan via email, sertakan pula **url link** ke repo project Github Anda._

#

### _**#HappyCoding**_

#### Gibox Digital Asia: reqruitment@giboxdigital.com*

[Website](https://www.giboxdigital.com/) |
[LinkedIn]() |
[GitHub]()) |
